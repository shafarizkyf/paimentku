<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reset-config', function(){
  Artisan::call('config:cache');
  Artisan::call('view:clear');
});

Route::get('/', function () {
  return view('landing/home');
})->name('home');

Route::get('about', function(){
  return view('landing/about');
})->name('about');

Route::get('blog', function(){
  return view('landing/blog');
})->name('blog');

Route::get('kebijakan-privasi', function(){
  return view('landing/kebijakan');
})->name('kebijakan_privasi');

Route::get('kebijakan-privasi/pdf', function(){
  return response()->file('assets/pdf/Kebijakan_Privasi_paimentku.pdf');
})->name('kebijakan_privasi.pdf');

Route::get('syarat-ketentuan', function(){
  return view('landing/kebijakan');
})->name('syarat_ketentuan');

Route::get('syarat-ketentuan/pdf', function(){
  return response()->file('assets/pdf/syarat_dan_ketentuan.pdf');
})->name('syarat_ketentuan.pdf');

Route::prefix('dashboard')->group(function(){
  Route::get('', 'DashboardController@index')->name('dashboard.index');
  Route::get('ax-graph', 'DashboardController@volume')->name('dashboard.volume');
});

Route::post('withdraw', 'WithdrawController@create')->name('withdraw.create');
Route::get('transaction', 'TransactionController@index')->name('transaction.index');
Route::get('billing', 'BillingController@index')->name('billing.index');

Route::prefix('account')->group(function(){
  Route::get('profile', 'UserController@profile')->name('profile.index');
  Route::post('profile', 'UserController@saveProfile')->name('profile.update');

  Route::get('ip-whitelist', 'UserController@ipWhitelist')->name('ip_whitelist.index');
  Route::post('ip-whitelist', 'UserController@addIpWhitelist')->name('ip_whitelist.add');

  Route::get('user-management', 'UserController@index')->name('user_management.index');
  Route::post('user-management', 'UserController@addUser')->name('user_management.add');
  Route::post('user-management/update', 'UserController@updateUser')->name('user_management.update');
  Route::post('user-management/delete', 'UserController@deleteUser')->name('user_management.delete');

  Route::get('log-activity', 'UserController@logActivity')->name('log_activity.index');
});

Route::prefix('settings')->group(function(){
  Route::get('general-settings', 'SettingsController@index')->name('general_settings.index');
  Route::post('general-settings/update-business', 'SettingsController@updateBusinessSettings')->name('general_settings.updateBusinessSettings');
  Route::post('general-settings/update-interface', 'SettingsController@updateInterface')->name('general_settings.updateInterface');

  Route::get('access-keys', 'SettingsController@accessKeys')->name('access_keys.index');

  Route::get('configuration', 'SettingsController@configuration')->name('configuration.index');
  Route::post('configuration', 'SettingsController@updateConfiguration')->name('configuration.update');

  Route::get('email-notification', 'SettingsController@emailNotification')->name('email_notification.index');
  Route::post('email-notification', 'SettingsController@updateEmailNotification')->name('email_notification.update');

  Route::get('daily-report', 'SettingsController@dailyReport')->name('daily_report.index');
  Route::post('daily-report', 'SettingsController@addEmailDailyReport')->name('daily_report.addEmail');
  Route::post('daily-report/delete', 'SettingsController@deleteEmailDailyReport')->name('daily_report.deleteEmail');
});

Route::get('login', 'LoginController@index')->name('login.index');
Route::post('login', 'LoginController@login')->name('login');

Route::get('register', 'RegistrationController@index')->name('register.index');
Route::post('register', 'RegistrationController@create')->name('register.create');
Route::get('activation', 'RegistrationController@verify')->name('register.verify');

Route::prefix('service')->group(function(){
  Route::get('payment-point', 'PaymentPointController@index')->name('service.payment_point.index');
  Route::post('payment-point', 'PaymentPointController@create')->name('service.payment_point.create');

  Route::get('bank-virtual-account', 'VirtualAccountController@index')->name('service.bank_virtual_account.index');
  Route::post('bank-virtual-account', 'VirtualAccountController@create')->name('service.bank_virtual_account.create');

  Route::get('debit-credit-card', 'CreditCardController@index')->name('service.debit_credit_card.index');
  Route::post('debit-credit-card', 'CreditCardController@create')->name('service.debit_credit_card.create');

  Route::get('ewallet', 'WalletController@index')->name('service.ewallet.index');
  Route::post('ewallet', 'WalletController@create')->name('service.ewallet.create');

  Route::get('internet-banking', 'BankingController@index')->name('service.internet_banking.index');
  Route::post('internet-banking', 'BankingController@create')->name('service.internet_banking.create');
});

Route::prefix('pricing')->group(function(){
  Route::get('business-enterprise', function(){
    return view('landing/pricing/business_enterprise');
  })->name('pricing.business_enterprise');
});

Route::get('support', function () {
  return view('landing/support');
})->name('support');

Route::get('socket', 'RegistrationController@test');
<?php

return [
  //'host'  => '183.91.70.178',
  'host'  => '223.25.100.50',
  'port'  => 53333,
  'com'   => [
    'register'            => 'REGIST',
    'login'               => 'LOGIN',
    'set_payment_point'   => 'SET_PAY_POINT',
    'set_virtual_account' => 'SET_VA',
    'set_credit_card'     => 'SET_CC',
    'set_wallet'          => 'SET_WALLET',
    'set_banking'         => 'SET_BANKING',          
    'register_confirm'    => 'CONF_REGIST_MASTER',
    'dashboard'           => 'VIEW_DASH',
    'transaction_volume'  => 'VIEW_TRANS_VOL',
    'history_pull'        => 'HIST_PULL',
    'history_transaction' => 'HIST_TRANS',
    'billing'             => 'GET_BILL',
    'set_profile'         => 'SET_PROFIL',
    'set_pass'            => 'SET_PASS',
    'set_ip_whitelist'    => 'SET_IP_WHITE',
    'add_sub_user'        => 'ADD_SUB_USER',
    'edit_sub_user'       => 'EDIT_SUB_USER',
    'delete_sub_user'     => 'DELETE_SUB_USER',
    'sub_user'            => 'GET_LIST_SUB',
    'log_user'            => 'GET_LOG',
    'set_general'         => 'SET_GENERAL',
    'access_key'          => 'GET_KEY',
    'set_url'             => 'SET_URL',
    'set_notif'           => 'SET_NOTIF',
    'add_email_report'    => 'ADD_REP_EMAIL',
    'email_report'        => 'GET_EMAIL_REP',
    'delete_email_report' => 'DEL_EMAIL_REP',
    'set_logo'            => 'SET_LOGO',
    'view_logo'           => 'VIEW_LOGO'
  ],
  'response' => [
    'success'     => '0000',
    'error'       => '0001',
    'format'      => '0002',
    'notRegister' => '0003',
    'cutoff'      => '0009'
  ]
];
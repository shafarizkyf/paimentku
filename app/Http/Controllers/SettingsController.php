<?php

namespace App\Http\Controllers;

use App\MyClass\Reusable;
use App\MyClass\Socket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingsController extends Controller{

  public function index(){
    return view('admin/settings/general_settings');
  }

  public function accessKeys(){
    $auth = session()->get('userLogin');

    $message = ['email_user'=>$auth->email];
    $accessKey = Socket::send($message, 'api.com.access_key');

    if($accessKey->resultcode != config('api.response.success')){
      $accessKey = new \stdClass();
      $accessKey->id = '-';
      $accessKey->client = '-';
      $accessKey->server = '-';
    }

    return view('admin/settings/access_keys', compact('accessKey'));
  }

  public function configuration(){
    return view('admin/settings/configuration');
  }

  public function emailNotification(){
    return view('admin/settings/email_notifications');
  }

  public function dailyReport(){
    $auth = session()->get('userLogin');

    $message = ['email_user'=>$auth->email];
    $data = Socket::send($message, 'api.com.email_report');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }

    $emailReport = array();
    for($i=1; $i<=$data->tot_data; $i++){
      $email = "email{$i}";
      $emailReport[] = ['email'=>$data->$email];
    }

    return view('admin/settings/daily_report', compact('emailReport'));
  }

  public function updateBusinessSettings(Request $request){

    $auth = session()->get('userLogin');
    $message = array_merge($request->except('_token'),[
      'email_user' => $auth->email
    ]);

    $result = Socket::send($message, 'api.com.set_general');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function updateInterface(Request $request){
    $auth = session()->get('userLogin');

    $request->validate([
      'label' => 'required|min:3',
      'logo'  => 'max:1000|mimes:png,jpg'
    ]);

    $logo     = $request->file('logo');
    $filename = $logo->getClientOriginalName() . time();
    $filename = md5($logo).'.'.$logo->getClientOriginalExtension();
    $host     = $request->getHost();
    $path     = 'uploads/logo';

    $logo->move($path, $filename);

    $message = [
      'email_user'  => $auth->email,
      'label'       => $request->label,
      'url'         => "{$host}/$path/$filename"
    ];

    $result = Socket::send($message, 'api.com.set_logo');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function updateConfiguration(Request $request){
    $auth = session()->get('userLogin');
    $message = array_merge($request->except('_token'),[
      'email_user' => $auth->email
    ]);
    
    $result = Socket::send($message, 'api.com.set_url');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function updateEmailNotification(Request $request){

    $auth = session()->get('userLogin');
    $message = array_merge($request->except('_token'),[
      'email_user' => $auth->email
    ]);

    $message['send_cust'] = $request->send_cust ? 1 : 0;
    $message['send_me'] = $request->send_me ? 1 : 0;
    
    $result = Socket::send($message, 'api.com.set_notif');
    $session = Socket::handleResponse($result);
    
    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function addEmailDailyReport(Request $request){
    $auth = session()->get('userLogin');
    $message = array_merge($request->except('_token'),[
      'email_user' => $auth->email
    ]);

    $result = Socket::send($message, 'api.com.add_email_report');
    $session = Socket::handleResponse($result);
    
    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function deleteEmailDailyReport(Request $request){
    $auth = session()->get('userLogin');
    $message = array_merge($request->except('_token'),[
      'email_user' => $auth->email
    ]);

    $result = Socket::send($message, 'api.com.delete_email_report');
    $session = Socket::handleResponse($result);
    
    return redirect()->back()->with($session['session'], $session['message']);
  }

}

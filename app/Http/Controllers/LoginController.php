<?php

namespace App\Http\Controllers;

use App\MyClass\Socket;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class LoginController extends Controller{
  
  public function index(){
    return view('landing.login');
  }

  public function login(Request $request){

    $request->validate([
      'id_user' => 'required',
      'pass'    => 'required'
    ],[
      'id_user.required'  => 'Username required',
      'pass.required'     => 'Password required'
    ]);

    $counter = Reusable::makeCounter();
    $password = "{$request->pass}$counter";

    $data = [
      'com'     => config('api.com.login'),
      'user'    => $request->id_user,
      'pass'    => md5($password),
      'code'    => '191.168.100.3',
      'ipku'    => '191.168.100.3',
      'counter' => $counter,
    ];

    $socket = new Socket();
    $socket->create();
    $socket->connect();
    $socket->write($data);
    $result = $socket->read();
    $socket->close();

    if($result->resultcode == config('api.response.success')){
      $userLogin = (object) [
        'email' => $request->id_user,
        'nama'  => $result->nama,
        'id'    => $result->id
      ];

      session()->put('userLogin', $userLogin);
      return redirect(route('dashboard.index'));

    }else{
      $response['status'] = 'danger';
      $response['message'] = "Terjadi kesalahan teknis [{$result->resultcode}]";
    }

    return redirect()->back()->with($response['status'], $response['message']);
  }

}

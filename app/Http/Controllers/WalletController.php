<?php

namespace App\Http\Controllers;

use App\MyClass\Reusable;
use App\MyClass\Socket;
use Illuminate\Http\Request;

class WalletController extends Controller{

  public function index(){
    return view('landing/services/ewallet');
  }

  public function create(Request $request){
    $request->validate([
      'nama'  => 'required',
      'email' => 'required',
      'telp'  => 'required',
      'situs' => 'required',
      'g-recaptcha-response' => 'required|captcha'
    ], [
      'g-recaptcha-response.required' => 'Klik captcha berikut'
    ]);

    $data = array_merge($request->all(),[
      'com'         => config('api.com.set_wallet'),
      'counter'     => Reusable::makeCounter(),
      'email_user'  => $request->email,
    ]);

    $socket = new Socket();
    $socket->create();
    $socket->connect();
    $socket->write($data);
    $result = $socket->read();
    $socket->close();

    $response = Reusable::makeResponseMessage($result, 'E-Wallet');    
    return redirect()->back()->with($response['status'], $response['message']);
  }

}

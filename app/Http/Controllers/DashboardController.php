<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\MyClass\Socket;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class DashboardController extends Controller{

  public function index(Request $request){

    if(!session()->get('userLogin')){
      return redirect(route('login'));
    }

    $summary = $this->summary();
    $history = $this->history();

    return view('admin/dashboard', compact('summary', 'history'));
  }

  public function summary(){
    $auth = session()->get('userLogin');
    
    $message = ['email_user' => $auth->email];
    $data = Socket::send($message, 'api.com.dashboard');

    if($data->resultcode != config('api.response.success')){
      $data = new \stdClass ();
      $data->tot_volume = 0;
      $data->tot_trans = 0;
    }else{
      $data->tot_volume = Reusable::formatMoney($data->tot_volume, 'Rp');
      $data->tot_trans = Reusable::formatMoney($data->tot_trans);
    }

    return $summary = $data;
  }

  public function volume(Request $request){
    $auth = session()->get('userLogin');

    $date = date('Y-m-d');
    if($request->date){
      $date = date('Y-m-d', strtotime("{$request->date} days"));
      $dateRequest = Carbon::parse($date);
    }


    $message = [
      'email_user' => $auth->email,
      'tanggal' => $date
    ];

    $data = Socket::send($message, 'api.com.transaction_volume');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }

    $volume = array();
    for($i=1; $i<31; $i++){
      $no       = sprintf('%02d', $i);
      $jumlah   = "trans_{$no}";
      $tanggal  = "{$data->tanggal}-{$no}";
      $today    = Carbon::parse($tanggal);
      
      if(!property_exists($data, $jumlah)){
        continue;
      }

      if($request->date && $dateRequest->lessThan($today)){
        continue;
      }

      $volume[] = [
        'jumlah'  => $data->$jumlah,
        'tanggal' => $today->format('d/m'),
      ];
    }

    return $volume;
  }

  public function history(){
    $auth = session()->get('userLogin');
    
    $message = [
      'email_user'  => $auth->email, 
      'dari_tgl'    => '2019-01-01',
      'sampai_tgl'  => '2019-12-31'
    ];
    
    $data = Socket::send($message, 'api.com.history_pull');

    if($data->resultcode != config('api.response.success')){
      return [];
    }

    $history = array();
    for($i=1; $i<=$data->tot_data; $i++){
      $bank         = "bank{$i}";
      $keterangan   = "keterangan{$i}";
      $tanggal      = "tgl{$i}";
      $rekening     = "norek{$i}";
      
      $history[] = [
        'bank'        => $data->$bank,
        'keterangan'  => $data->$keterangan,
        'tanggal'     => Reusable::parseDate($data->$tanggal)->format('d-m-Y'),
        'rekening'    => $data->$rekening
      ];
    }

    return $history;
  }

}

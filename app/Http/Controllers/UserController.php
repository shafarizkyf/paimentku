<?php

namespace App\Http\Controllers;

use App\MyClass\Reusable;
use App\MyClass\Socket;
use Illuminate\Http\Request;

class UserController extends Controller{

  public function index(){
    $auth = session()->get('userLogin');

    $dataUser = ['email_user' => $auth->email];
    $data = Socket::send($dataUser, 'api.com.sub_user');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }

    $users = array();
    for($i=1; $i<=$data->tot_data; $i++){
      $email    = "email{$i}";
      $nama     = "nama{$i}";
      $posisi   = "posisi{$i}";
      $tanggal  = "tgl_masuk{$i}";

      $namaPosisi = '';
      if($data->$posisi == 1){
        $namaPosisi = 'Admin';
      }elseif($data->$posisi == 2){
        $namaPosisi = 'Billing';
      }elseif($data->$posisi == 3){
        $namaPosisi = 'Adder';
      }

      $users[] = [
        'email'       => $data->$email,
        'tanggal'     => $data->$tanggal,
        'posisi'      => $namaPosisi,
        'id_posisi'   => $data->$posisi,
        'nama'        => $data->$nama,
      ];
    }

    return view('admin/account/user_management', compact('users'));
  }

  public function profile(){
    return view('admin/account/profile');
  }

  public function ipWhitelist(){
    return view('admin/account/ip_whitelist');
  }

  public function logActivity(Request $request){
    $auth = session()->get('userLogin');

    $dataLog = [
      'email_user'  => $auth->email,
      'email_sub'   => $auth->email,
      'dari_tgl'    => '2018-01-01',
      'sampai_tgl'  => '2018-12-31'
    ];

    $data = Socket::send($dataLog, 'api.com.log_user');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }

    $activity = array();
    for($i=1; $i<=$data->tot_data; $i++){
      $email    = "email_sub{$i}";
      $tanggal  = "tgl{$i}";
      $ip       = "ip_sub{$i}";
      $log      = "log{$i}";

      if($request->email && strpos(strtolower($data->$email), $request->email) === false){
        continue;
      }
      
      if($request->ip && strpos($data->$ip, $request->ip) === false){
        continue;
      }

      if($request->activity && strpos(strtolower($data->$log), $request->activity) === false){
        continue;
      }
        
      $activity[] = [
        'email'     => $data->$email,
        'tanggal'   => Reusable::parseDate($data->$tanggal)->format('d-m-Y'),
        'ip'        => $data->$ip,
        'log'       => $data->$log
      ];
    }

    return view('admin/account/log_activity', compact('activity'));
  }

  public function saveProfile(Request $request){

    $auth = session()->get('userLogin');

    //update profile
    $dataUpdateProfile = [
      'email_user'  => $auth->email,
      'nama'        => $request->nama,
      'jenkel'      => $request->jenkel,
      'hp'          => $request->hp,
      'website'     => $request->website
    ];

    $updateProfile = Socket::send($dataUpdateProfile, 'api.com.set_profile');
    $session = Socket::handleResponse($updateProfile);

    //update password
    $updatePassword = [];
    if($request->new_pass && $request->conf_pass){
      $counter = Reusable::makeCounter();
      $dataUpdatePassword = [
        'email_user'  => $auth->email,
        'old_pass'    => md5("{$request->old_pass}{$counter}"),
        'new_pass'    => $request->new_pass,
        'conf_pass'   => $request->conf_pass
      ];

      $updatePassword = Socket::send($dataUpdatePassword, 'api.com.set_pass');
      $session = Socket::handleResponse($updatePassword);
    }

    // $message = $sessionUpdateProfile['message'];
    // $session = $sessionUpdateProfile['session'];

    // if($sessionUpdateProfile['code'] !== config('api.response.success')){
    //   $message = "Update Profile Gagal [{$updateProfile->resultcode}] ";
    //   $session = 'danger';
    // }

    // if($sessionUpdatePassword['code'] !== config('api.response.success')){
    //   $message .= "Update Password Gagal [{$updatePassword->resultcode}]";
    //   $session = 'danger';
    // }

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function addIpWhitelist(Request $request){
    $auth = session()->get('userLogin');
    
    $addWhitelist = [
      'email_user'  => $auth->email,
      'ip_now'      => $request->ip(),
      'ip_add'      => $request->ip_add
    ];

    $result = Socket::send($addWhitelist, 'api.com.set_ip_whitelist');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function addUser(Request $request){
    $auth = session()->get('userLogin');

    $addUserData = $request->except('_token');
    $hak = implode(',', $request->hak);

    $addUserData['email_user'] = $auth->email;
    $addUserData['hak'] = $hak;

    $result = Socket::send($addUserData, 'api.com.add_sub_user');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }

  public function updateUser(Request $request){
    $auth = session()->get('userLogin');

    $addUserData = $request->except('_token');
    $hak = implode(',', $request->hak);

    $addUserData['email_user'] = $auth->email;
    $addUserData['hak'] = $hak;

    $result = Socket::send($addUserData, 'api.com.edit_sub_user');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);    
  }

  public function deleteUser(Request $request){
    $auth = session()->get('userLogin');

    $data = [
      'email_user'  => $auth->email,
      'email_sub'   => $request->email_sub
    ];

    $result = Socket::send($data, 'api.com.delete_sub_user');
    $session = Socket::handleResponse($result);

    return redirect()->back()->with($session['session'], $session['message']);
  }


}

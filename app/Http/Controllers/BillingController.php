<?php

namespace App\Http\Controllers;

use App\MyClass\Reusable;
use App\MyClass\Socket;
use Illuminate\Http\Request;

class BillingController extends Controller{

  public function index(){
    $auth = session()->get('userLogin');
    
    $message = [
      'email_user' => $auth->email, 
      'dari_tgl' => '2019-01-01', 
      'sampai_tgl'=>'2019-12-31'
    ];

    $data = Socket::send($message, 'api.com.billing');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }
    
    $billing = array();
    for($i=1; $i<=$data->tot_data; $i++){
      $id         = "order_id{$i}";
      $total      = "total{$i}";
      $keterangan = "ket{$i}";
      $refund     = "refund{$i}";
      $tanggal    = "tgl{$i}";
      $jumlah     = "jumlah{$i}";
      $admin      = "admin{$i}";

      $billing[] = [
        'id_order'    => $data->$id,
        'total'       => Reusable::formatMoney($data->$total, 'Rp'),
        'keterangan'  => $data->$keterangan,
        'refund'      => Reusable::formatMoney($data->$refund, 'Rp'),
        'admin'       => Reusable::formatMoney($data->$admin, 'Rp'),
        'jumlah'      => Reusable::formatMoney($data->$jumlah, 'Rp'),
        'tanggal'     => Reusable::parseDate($data->$tanggal)->format('d-m-Y'),
      ];
    }

    return view('admin/billing', compact('billing'));
  }

}

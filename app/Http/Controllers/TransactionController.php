<?php

namespace App\Http\Controllers;

use App\MyClass\Reusable;
use App\MyClass\Socket;
use Illuminate\Http\Request;

class TransactionController extends Controller{
  
  public function index(){
    $auth = session()->get('userLogin');
    
    $message = [
      'email_user' => $auth->email, 
      'dari_tgl' => '2019-01-01', 
      'sampai_tgl'=>'2019-12-31'
    ];

    $data = Socket::send($message, 'api.com.history_transaction');

    if($data->resultcode !== config('api.response.success')){
      return [];
    }

    $transaction = array();
    for($i=1; $i<=$data->tot_data; $i++){
      
      $id         = "order_id{$i}";
      $keterangan = "ket{$i}";
      $type       = "type{$i}";
      $status     = "status{$i}";
      $jumlah     = "jumlah{$i}";
      $tanggal    = "tgl{$i}";

      $transaction[] = [
        'id_order'    => $data->$id,
        'keterangan'  => $data->$keterangan,
        'type'        => $this->type($data->$type),
        'status'      => $this->status($data->$status),
        'jumlah'      => Reusable::formatMoney($data->$jumlah, 'Rp'),
        'tanggal'     => Reusable::parseDate($data->$tanggal)->format('d-m-Y')
      ];
    }

    return view('admin/transaction', compact('transaction'));
  }

  public function status($id){
    switch($id){
      case 0:
        $status = 'Pending';
      break;

      case 1:
        $status = 'Sukses';
      break;

      case 2:
        $status = 'Gagal';
      break;

      default:
        $status = 'Tidak Diketahui';
    }

    return $status;
  }

  public function type($id){
    switch($id){
      case 1:
        $status = 'Virtual Account';
      break;

      case 2:
        $status = 'Debit/Credit Card';
      break;

      case 3:
        $status = 'E-Wallet';
      break;

      case 4:
        $status = 'Internet Banking';
      break;

      case 5:
        $status = 'Davestpay Payment Point';
      break;

      default:
        $status = 'Tidak Diketahui';
    }

    return $status;
  }

}

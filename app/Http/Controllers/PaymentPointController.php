<?php

namespace App\Http\Controllers;

use Validator;
use App\MyClass\Socket;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class PaymentPointController extends Controller{

  public function index(){
    return view('landing/services/payment_point');
  }

  public function create(Request $request){

    $request->validate([
      'nama'  => 'required',
      'email' => 'required',
      'telp'  => 'required',
      'situs' => 'required',
      'g-recaptcha-response' => 'required|captcha'
    ], [
      'g-recaptcha-response.required' => 'Klik captcha berikut'
    ]);

    $data = array_merge($request->all(),[
      'com'         => config('api.com.set_payment_point'),
      'counter'     => Reusable::makeCounter(),
      'email_user'  => $request->email,
    ]);

    $socket = new Socket();
    $socket->create();
    $socket->connect();
    $socket->write($data);
    $result = $socket->read();
    $socket->close();

    $response = Reusable::makeResponseMessage($result, 'PaimentPoint');    
    return redirect()->back()->with($response['status'], $response['message']);
  }

}

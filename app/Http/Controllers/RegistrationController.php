<?php

namespace App\Http\Controllers;

use Validator;
use App\MyClass\Socket;
use App\MyClass\Reusable;
use Illuminate\Http\Request;

class RegistrationController extends Controller{

  public function index(){
    return view('landing/register');
  }

  public function create(Request $request){
    $request->validate([
      'n_dagang'  => 'required',
      'n_usaha'   => 'required',
      'nama'      => 'required',
      'email'     => 'required',
      'jenkel'    => 'required',
      'telp'      => 'required',
      'situs'     => 'required',
      'pass'      => 'required',
      'repass'    => 'required|same:pass',
      'g-recaptcha-response' => 'required|captcha'
    ], [
      'n_dagang.required' => 'Nama dagang required',
      'n_usaha.required'  => 'Nama usaha required',
      'nama.required'     => 'Nama Anda required',
      'email.required'    => 'Email Anda required',
      'jenkel.required'   => 'Jenis kelamin required',
      'telp'              => 'Nomor telepon required',
      'situs'             => 'Situs website required',
      'pass.required'     => 'Password required',
      'repass.required'   => 'Konfirmasi password required',
      'g-recaptcha-response.required' => 'Klik captcha berikut'
    ]);

    $result = Socket::send($request, 'api.com.register');

    if($result->resultcode == config('api.response.success')){
      $response['status'] = 'success';
      $response['message'] = '';
    }else{
      $response['status'] = 'danger';
      $response['message'] = "Terjadi kesalahan [$result->resultcode]";
    }

    return redirect()->back()->with($response['status'], $response['message']);

  }

  public function verify(Request $request){

    $data = [
      'com'     => config('api.com.register_confirm'),
      'email'   => $request->emv,
      'unik'    => $request->authcode,
      'counter' => Reusable::makeCounter()
    ];

    $socket = new Socket();
    $socket->create();
    $socket->connect();
    $socket->write($data);
    $result = $socket->read();
    $socket->close();
    
    if($result->resultcode == config('api.response.success')){
      $response['status'] = 'success';
      $response['message'] = 'Berhasil mem-verifikasi email. Silahkan Login';
    }else{
      $response['status'] = 'danger';
      $response['message'] = "Terjadi kesalahan [$result->resultcode]";
    }
    
    return redirect(route('login'))->with($response['status'], $response['message']);
  }

  public function test(){
    $socket = new Socket();
    $socket->create();
    return [$socket->connect()];
  }

}

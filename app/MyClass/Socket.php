<?php

namespace App\MyClass;

use App\MyClass\Reusable;
use Illuminate\Http\Request;

class Socket {

  public $_host;
  public $_port;
  public $_socket;

  function __construct(){
    $this->_host = config('api.host');
    $this->_port = config('api.port');
  }

  public function create(){
    return $this->_socket = socket_create(AF_INET, SOCK_STREAM, 0);
  }

  public function connect(){
    return socket_connect($this->_socket, $this->_host, $this->_port);
  }

  public function write($message, $format=true){
    $message = json_encode($message);
    if($format) $message = "{$message}\n";

    return socket_write($this->_socket, $message, strlen($message));
  }

  public function read(){
    return json_decode(socket_read($this->_socket, 100000, PHP_NORMAL_READ));
  }

  public function close(){
    return socket_close($this->_socket);
  }

  public static function send($request, $com){

    if($request instanceof Request){
      $request = $request->all();
    }

    $data = array_merge($request, [
      'com'     => config($com),
      'counter' => Reusable::makeCounter()
    ]);

    $socket = new self();
    $socket->create();
    $socket->connect();
    $socket->write($data);
    $result = $socket->read();
    $socket->close();

    return $result;
  }

  public static function handleResponse($result){
    switch($result->resultcode){
      case config('api.response.success'):
        $data = ['session' => 'success', 'code' => $result->resultcode, 'message'=>'Berhasil'];
      break;
        
      case config('api.response.error'):
      case config('api.response.format'):
      case config('api.response.notRegister'):
      case config('api.response.cutoff'):
        $data = ['session' => 'danger', 'code' => $result->resultcode, 'message'=>"Error [{$result->resultcode}]"];
      break; 
    }

    return $data;
  }

}
<?php 

namespace App\MyClass;

use Carbon\Carbon;

class Reusable {

  public static function makeCounter($min=10000, $max=99999){
    return random_int($min, $max);
  }

  public static function makeResponseMessage($result, $subject=''){
    $response = array();
    if($result->resultcode == config('api.response.success')){
      $response['status'] = 'success';
      $response['message'] = "Berhasil mengaktifkan {$subject}";
    }else{
      $response['status'] = 'danger';
      $response['message'] = "Terjadi kesalahan teknis [{$result->resultcode}]";
    }

    return $response;
  }

  public static function formatMoney($value, $symbol='', $thousand='.', $decimal=','){
    return $symbol != '' 
    ? "{$symbol} ".number_format($value, 0, $decimal, $thousand) 
    : number_format($value, 0, $decimal, $thousand);
  }

  public static function parseDate($date){
    return Carbon::parse($date);
  }

}

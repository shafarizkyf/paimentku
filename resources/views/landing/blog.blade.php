<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PaimentKu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Bootstap -->
    <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font awesome -->
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- My style -->
    <link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <section class="">
      <nav class="navbar navbar-fixed-top custom-navbar" style="height: 60px;">
        <div class="container xsNoPadding">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbarBrandHome2" href="index.php"><img class="" src="assets/image/logoPaymentku.svg"></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse navbar-xs" style="padding-left:0px; z-index: 100; color: #fefefe">
            @include('layouts.components.navbar')
          </div>
        </div>
      </nav>
    </section>
    <section class="content" style="">
      <div class="bannerHome4">
        <div class="bannerBox" style="background-image: url('assets/image/bannerblog.jpg');background-repeat: no-repeat;background-size: 100% 100%;">
          <div class="container">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
              <div class="col-lg-8 col-sm-7 col-md-8 col-xs-12 layananBox">
                <div class="layananBoxText2 vhCenter">Paymentku Blog - Belajar Bisnis Online, eCommerce, & Tips Internet Marketing</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="daftar" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-header" style="padding: 5px 15px;border: none">
              <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top: 0px">
              <div class="formBlock centered" style="padding: 10px 15px">
                <p><b>Silahkan Isi Form Dibawah Ini</b></p>
                <div class="form-group" style="margin-top: 20px">
                  <input class="form-control" type="text" name="" placeholder="Nama Anda">
                </div>
                <div class="form-group" style="">
                  <input class="form-control" type="text" name="" placeholder="No. Handphone">
                </div>
                <div class="form-group" style="">
                  <input class="form-control" type="text" name="" placeholder="E-mail">
                </div>
                <div class="form-group" style="">
                  <input class="form-control" type="text" name="" placeholder="Website">
                </div>
                <div class="form-group">
                  <div style="height: 40px;width: 80%; border: thin grey solid">Capthca Google</div>
                </div>
                <div class="form-group" style="margin-top: 20px">
                  <a type="button" href="successdaftar.php" class="btn btn-block btn-primary" style="border-radius: 0px">Proses</a>
                  <a type="button" href="successdaftar.php" class="btn btn-block btn-default" style="border-radius: 0px">Batal</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container" style="min-height: 500px;" >
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 xsNoPadding">
          <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 fullheight blogcontent">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox">
              <div class="small-text">Tips & trick </div>
              <div class="litle-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox">
              <div class="small-text">Tips & trick </div>
              <div class="litle-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox">
              <div class="small-text">Tips & trick </div>
              <div class="litle-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 xsNoPadding">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight  blogcontent">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 xsNoPadding">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight  blogcontent">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 xsNoPadding">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight  blogcontent">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fullheight blogcontent" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide imgbox2">
              <img src="https://via.placeholder.com/150">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 captionbox2 noPaddingSide ">
              <div class="small-text captionColorBlue">Tips & trick </div>
              <div class="litle-title captionColorBlue">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
              <div class="caption2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 xsNoPadding">
          <div class="p20 noPaddingSide centered" style="border-top: 4px solid grey;">
            <p class="pull-left"><span class="fa fa-long-arrow-left fa-lg"></span>&ensp;Newer Post</p>
            Page 1 of 44
            <p class="pull-right">Older Post &ensp;<span  class="fa fa-long-arrow-right fa-lg"></span></p>
          </div>
        </div>
      </div>
    </section>
    <section class="footer">
      <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt30 xsNoPadding">
          <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 footerLink" style="color: #fff; margin-bottom: 20px">
            <p><b>Biropay</b><br></p>
            <a href="#">Tentang Kami</a><br>
            <a href="#">Blog</a>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footerLink" style="color: #fff;margin-bottom:20px">
            <p><b>Bantuan</b><br></p>
            <a href="#">Syarat dan Ketentuan</a><br>
            <a href="#">Kebijakan Privasi</a><br>
            <a href="#">Hubungi Kami</a>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 footerLink pull-right xs-left tar xs-tal" style="color: #f6f6f6;">
            Temukan kami &ensp;&ensp;
            <ul class="social-network social-circle" style="display: inline-block;">
              <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" class="icoFacebook" title="Instagram"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#" class="icoGoogle" title="Youtube"><i class="fa fa-youtube"></i></a></li>
              <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <div class="centered" style=" padding:20px">
              <div class="pull-right centered xs-center imagePlayStore">
                Dapatkan Aplikasi Mobile Paymentku
                <a href="#"><img src="assets/image/google.png" style="height: 30px"></a>
                <a href="#"><img src="assets/image/appstore.png" style="height: 30px"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/myJs.js') }}"></script>
  </body>
</html>
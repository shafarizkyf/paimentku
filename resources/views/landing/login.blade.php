
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8" />
	    <title>PaimentKu</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <!-- Bootstap -->
      <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
      <!-- Font awesome -->
      <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
      <!-- My style -->
      <link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
	    <style type="text/css">
	    	html,body {
	    		height: 100%;
	    		background-image: linear-gradient(to right, #06a3a6, #025657);
	    	}
	    	.formBlock .form-control{
	    		height: 36px !important;
	    	}
	    	.frm{
	    		height: 350px;

	    	}
	    </style>
	</head>
	<body  style="">
		<section class="">
			<div class="container">
				<div class="frm"  style="background-image: linear-gradient(to right bottom , #75c8ae, #3c9289); height: 400px;">
					<div class="col-xs-12">
            <form action="{{ route('login') }}" method="POST">
              {{ csrf_field() }}
              <div class="formBlock centered">
                <img src="{{ asset('assets/image/logoPaymentku.svg') }}" style="width: 70%">
                @if(Session::has('success'))
                  <h6 class="text-center response text-success">{{ Session::get('success') }}</h6>
                @elseif(Session::has('danger'))
                  <h6 class="text-center response text-danger">{{ Session::get('danger') }}</h6>
                @elseif(Session::has('warning'))
                  <h6 class="text-center response text-warning">{{ Session::get('warning') }}</h6>
                @endif

                <div class="input-group" style="margin-top: 20px;margin-bottom: 20px">
                  <span class="input-group-addon" style="background-color: #0e6473"><i class="fa fa-user" style="color: #fff"></i></span>
                  <input class="form-control" type="text" name="id_user" placeholder="Username" required>
                  @if($errors->has('id_user'))
                    <span class="text-danger">{{ $errors->first('id_user') }}</span>
                  @endif            
                </div>
                <div class="input-group" style="margin-bottom: 10px">
                  <span class="input-group-addon" style="background-color: #0e6473"><i class="fa fa-lock" style="color: #fff"></i></span>
                  <input class="form-control" type="password" name="pass" placeholder="Password" required>
                  @if($errors->has('pass'))
                    <span class="text-danger">{{ $errors->first('pass') }}</span>
                  @endif            
                </div>
                <div class="form-group" style="text-align: left;color: #fff">
                  <label class="radio-inline"><input type="radio" name="optradio" checked>Remember Me</label>
                  <a class="pull-right" href="" style="color: #fff">Forgot Password</a>
                </div>
                <div class="form-group" style="margin-top: 50px">
                  <button class="btn btn-block btn-peach">Login</button>
                </div>
              </div>
            </form>
					</div>
				</div>
			</div>
		</section>


    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/myJs.js') }}"></script>
	</body>
</html>
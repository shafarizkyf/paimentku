@extends('layouts.landing')

@section('css-bottom')
<style>
  .MT1000{
    margin-top: -800px;
  }
</style>
@endsection

@section('content')
<section class="content" style="">
  <div class="bannerHome">
  </div>
</section>
<section class="" style="background-color: #fff">
  <div class="container" style="">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 MT500 MT1000">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 boxHome centered" style="padding: 10px 50px 60px 50px; height: auto;">
        <div style="font-size: 40px;font-weight: 600;color: #ff6600; padding-bottom: 20px">Kebijakan Privasi</div>
        <div class="embed-responsive embed-responsive-16by9" style="height:auto; min-height: 500px; width: 100%;">
          <object data="{{ asset('assets/pdf/syarat_dan_ketentuan.pdf') }}" type="application/pdf" width="100%" height="100%" style="border-bottom:5px solid grey">
          Your browser is not capable of displaying PDF file. Please download the PDF file via the following <a href="{{ route('syarat_ketentuan.pdf') }}"> Link</a>
          </object>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
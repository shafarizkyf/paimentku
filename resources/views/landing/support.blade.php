@extends('layouts.landing_nav_white')

@section('content')
		<section class="content" style="min-height: 500px;">
			<div class="container">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mTB150 tablePricing xsNoPadding" style="border: none;">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  xsNoPadding" >
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p50 xsNoPadding" style="border: 1px grey solid;background-color: #fff;">
							<div class="col-lg-3 col-sm-4 col-md-3 col-xs-12 supportBox">
								<img src="{{ asset('assets/image/1.svg') }}" >
							</div>
							<div class="col-lg-9 col-sm-8 col-md-9 col-xs-12 supportBox">
								<div class="layananBoxText" style=" color: #303030;text-align: left;padding: 0px 20px">
									<div class="f16b">Daftar Akun Paymentku Anda</div>
									<div class="f16 justify">Daftarkan akun anda terlebih dahulu, untuk mendaftar akun Paymentku bisa Anda lihat pada halaman berikut <a href="{{ route('register.index') }}" style="color: #5cbbcc">>Daftar Sekarang<</a></div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-4 col-md-3 col-xs-12 supportBox">
								<img src="{{ asset('assets/image/2.svg') }}" >
							</div>
							<div class="col-lg-9 col-sm-8 col-md-9 col-xs-12 supportBox">
								<div class="layananBoxText" style=" color: #303030;text-align: left;padding: 0px 20px;margin-bottom: 70px">
									<div class="f16b">Integrasikan website Anda dengan Paymentku</div>
									<div class="f16 justify">Setelah berhasil terdaftar, login ke akun Paymentku Anda. Pilih tab API Menu Penjual, Anda akan mendapatkan API_KEY pada halaman tersebut. Selanjutnya Anda bisa mengikuti langkah-langkah integrasi Website pada halaman berikut <a href="#" style="color: #5cbbcc">>Cara Integrasi<</a></div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-4 col-md-3 col-xs-12 supportBox">
								<img src="{{ asset('assets/image/3.svg') }}" >
							</div>
							<div class="col-lg-9 col-sm-8 col-md-9 col-xs-12 supportBox">
								<div class="layananBoxText" style=" color: #303030;text-align: left;padding: 0px 20px;">
									<div class="f16b">Sertifikasi Belanja Aman Paymentku</div>
									<div class="f16 justify">Sertifikasi Paymentku kami lakukan kepada merchant untuk menambah kepercayaan pembeli sehingga meningkatkan konversi penjualan. Dengan adanya Sertifikat Belanja Aman Paymentku, menandakan bahwa Anda adalah merchant yang sudah melewati proses verifikasi Paymentku secara keseluruhan. <a href="{{ route('register.index') }}" style="color: #5cbbcc">>Daftar Sekarang<</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-support').addClass('active');
    });
  </script>
@endsection
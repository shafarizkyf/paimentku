@extends('layouts.landing')

@section('css-bottom')
<style type="text/css">
  .nav > li > a{
  color: #fefefe;
  }
  .paymentku{
  color: #ff6600; font-weight: 600
  }
  .visi{
  font-size: 16px;text-align: center;padding:40px 70px;
  }
  .tabalamat{
  min-height: 400px;
  }
  .alamatmidle{
  background-color: #fff;
  color: #000;
  }
  .sejarahtext{
  font-size: 16px;text-align: justify;
  }
  .textmiddle{
  color: #fff;
  font-size: 16px;text-align: center;padding:40px 70px; height: 80%
  }
  .alamatheight{
  height: 60%;
  }
  @media (max-width: 768px) {
  .alamat{
  height: 200px;
  }
  .tabalamat{
  min-height: 750px;
  }
  .tabalamat2{
  position: static;
  }
  .alamatmidle{
  background-color: transparent;
  color: #fff;
  }
  .tabsejarah{
  min-height: 750px;
  font-size: 12px;
  }
  .sejarahtext{
  font-size: 12px;text-align: justify;
  }
  .textmiddle{
  color: #fff;
  }
  .alamatlast{
  margin-top:15px
  }
  }
</style>
@endsection

@section('content')
<section class="content" style="">
  <div class="bannerHome3" style="background-color: rgba(4, 154, 156, 1); background-image: none">
    <div class="bannerBox">
      <div class="container">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="height: 50px; text-align: center;margin-bottom: -20px;">
              <img src="{{ asset('assets/image/Amazon_Arrows.svg') }}" style=" color: black; -webkit-transform: scaleY(-1);transform: scaleY(-1); height: 50px;">
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 layananBox" style="text-align: center;">
              <div class="layananBoxText visi xsNoPadding" style=";">Bertekad dan berkomitmen menjadi solusi pembayaran elektroni terlengkap di Indonesia dengan sistem yang handal. <br> Terus memantapkan diri menjadi perusahaan pembayaran elektronis yang terdepan dan dipercaya.</div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 layananBox" style="text-align: center;">
              <div class="layananBoxText visi xsNoPadding" style="">Menjadi fasilitator yang menghubungkan semua pembayaran digital dari merchant kepihak penerima pembayaran.</div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="height: 50px; text-align: center;">
              <img src="{{ asset('assets/image/Amazon_Arrows.svg') }}" style=" -webkit-transform: scaleX(-1);transform: scaleX(-1); height: 50px;margin-top: -20px;">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="content" style="">
  <div class="bannerHome3 tabalamat" style="background-color: #95a5a6; background-image: none;">
    <div class="bannerBox tabalamat2" style="background-color: #95a5a6">
      <div class="container">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 layananBox alamat" style="text-align: center;">
              <div class="layananBoxText xsNoPadding alamatheight" style="font-size: 16px;text-align: center;padding:40px 70px;">
                <span><i class="fa fa-street-view fa-4x"></i></span><br>JAKARTA
                <div style="font-size: 12px; margin-top:20px;">Jalan Casablanca Raya Kav.88, Lt.28C <br>Jakarta Selatan, DKI Jakarta, 12870</div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 layananBox alamat alamatmidl " style="text-align: center;">
              <div class="layananBoxText xsNoPadding alamatheight textmiddle " style="">
                <span><i class="fa fa-map-marker fa-4x"></i></span><br>MAKASAR
                <div style="font-size: 12px; margin-top:20px;">TravellerS Hotel Phinisi Lt.12 <br>Jalan Lamadukelleng No.59<br>Makassar, Sulawesi Selatan 90113</div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 layananBox alamat alamatlast " style="text-align: center;">
              <div class="layananBoxText xsNoPadding alamatheight" style="font-size: 16px;text-align: center;padding:40px 70px;">
                <span><i class="fa fa-map-signs fa-4x"></i></span><br>YOGYAKARTA
                <div style="font-size: 12px; margin-top:20px;">Genius Idea<br>Jl. Magelang No. 32-34, <br>Cokrodiningratan, Jetis,<br>Daerah Istimewa Yogyakarta, 55233</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@extends('layouts.landing_nav_white')

@section('content')
<section class="content" style="min-height: 500px">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mTB150 tablePricing xsNoPadding">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  xsNoPadding" >
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xsNoPadding">
          <table class="table col-xs-12" style="border: 1px grey solid">
            <tr style="height: 100px; font-size: 18px; color: #fff">
              <td style="background-color: #00b0b0 ; width: 33%"><b>FITUR</b></td>
              <td style="background-color: #0e6473 ; width: 33%">
                <b>BISNIS</b><br>
                <p>(Transaksi Debit)</p>
              </td>
              <td style="background-color: #0e6473 ; width: 33%">
                <b>ENTERPRISE</b><br>
                <p>(Semua Metode Pembayaran</p>
              </td>
            </tr>
            <tr class="bold">
              <td class="grey">Pendaftaran</td>
              <td>GRATIS!</td>
              <td>GRATIS!</td>
            </tr>
            <tr class="bold">
              <td class="grey">Penarikan Dana Ke Rekening Bank</td>
              <td>3 Hari</td>
              <td>Real Time</td>
            </tr>
            <tr class="bold">
              <td class="grey">Pembayaran Via Merchant Paymentku</td>
              <td><i class="fa fa-times fa-lg" style="color: red"></i></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
            <tr class="bold">
              <td class="grey">Pembayaran Via Paymentku Payment Point</td>
              <td><i class="fa fa-times fa-lg" style="color: red"></i></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
            <tr class="bold">
              <td class="grey">Pembayaran Transfer Bank Virtual Account</td>
              <td><i class="fa fa-times fa-lg" style="color: red"></i></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
            <tr class="bold">
              <td class="grey">Pembayaran Dengan Kartu Kredit/Debit</td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
            <tr class="bold">
              <td class="grey">E-Wallet</td>
              <td><i class="fa fa-times fa-lg" style="color: red"></i></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
            <tr class="bold">
              <td class="grey">Internet Banking</td>
              <td><i class="fa fa-times fa-lg" style="color: red"></i></td>
              <td><img src="{{ asset('assets/image/checklist.png') }}" style="height: 20px"></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-pricing').addClass('active');
    });
  </script>
@endsection
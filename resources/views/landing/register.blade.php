<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PaimentKu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Bootstap -->
    <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font awesome -->
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- My style -->
    <link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body  style="background-color: #737373;">
    <section class="">
      <div class="container">
        <div class="frm" style="height:auto; margin:20px auto">
          <div class="col-xs-12">
            
            @if(Session::has('success'))

              <div class="formBlock centered" style="position: relative;height: 500px;">
                <img src="assets/image/logoPaymentku.svg">
                <div class="form-group" style="margin-top: 20px">
                  <h3>Selamat</h3>
                  <p>Pendaftaran anda telah terkirim, Kami mengirimkan email konfirmasi ke email anda. Silahkan klik link yang terdapat di dalam email untuk konfirmasi akun Paymentku anda.</p> 
                </div>
                <div class="form-group" style="margin-top: 20px; position: absolute;bottom: 0; right: 0;left: 0; margin-left: 15px;margin-right: 15px;">
                  <a type="button" href="{{ route('login') }}" class="btn btn-block btn-peach" style="position: initial;">Tutup</a>
                </div>
              </div>
  
            @elseif(Session::has('danger'))
              <h3>Gagal</h3>
              <p>{{ Session::get('danger') }}</p> 

            @else
              <form action="{{ route('register.create') }}" method="POST">
                {{ csrf_field() }}
                <div class="formBlock centered">
                  <img src="{{ asset('assets/image/logoPaymentku.svg') }}">
                  <p><b>Daftar Akun Baru</b></p>
                  <div class="form-group" style="margin-top: 20px">
                    <input class="form-control" type="text" name="n_dagang" placeholder="Merk Dagang" required>
                    @if($errors->has('n_dagang'))
                      <span class="text-danger">{{ $errors->first('n_dagang') }}</span>
                    @endif
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="text" name="n_usaha" placeholder="Nama Perusahaan" required>
                    @if($errors->has('n_usaha'))
                      <span class="text-danger">{{ $errors->first('n_usaha') }}</span>
                    @endif
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" required>
                    @if($errors->has('nama'))
                      <span class="text-danger">{{ $errors->first('nama') }}</span>
                    @endif
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="text" name="email" placeholder="E-mail" required>
                    @if($errors->has('email'))
                      <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif                  
                  </div>
                  <div class="form-group" style="text-align: left;">
                    <label class="radio-inline"><input type="radio" value="Pria" name="jenkel" required>Pria</label>
                    <label class="radio-inline"><input type="radio" value="Wanita" name="jenkel" required>Wanita</label>
                    @if($errors->has('jenkel'))
                      <span class="text-danger">{{ $errors->first('jenkel') }}</span>
                    @endif                  
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="text" name="telp" placeholder="Nomor Handphone" required>
                    @if($errors->has('telp'))
                      <span class="text-danger">{{ $errors->first('telp') }}</span>
                    @endif                                    
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="text" name="situs" placeholder="Website" required>
                    @if($errors->has('situs'))
                      <span class="text-danger">{{ $errors->first('situs') }}</span>
                    @endif                                    
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="password" name="pass" placeholder="Password" required>
                    @if($errors->has('pass'))
                      <span class="text-danger">{{ $errors->first('pass') }}</span>
                    @endif                                    
                  </div>
                  <div class="form-group" style="">
                    <input class="form-control" type="password" name="repass" placeholder="Konfrim Password" required>
                    @if($errors->has('repass'))
                      <span class="text-danger">{{ $errors->first('repass') }}</span>
                    @endif                                    
                  </div>
                  <div class="form-group">
                    <div style="height: 70px;width: 50%;">
                      {!! NoCaptcha::display() !!}
                    </div>
                    @if($errors->has('g-recaptcha-response'))
                      <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif                                    
                  </div>
                  <div class="form-group" style="margin-top: 20px">
                    <button class="btn btn-block btn-peach">Daftar</button>
                  </div>
                </div>
              </form>              
            @endif
            
          </div>
        </div>
      </div>
    </section>
    {!! NoCaptcha::renderJs() !!}
    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/myJs.js') }}"></script>
  </body>
</html>
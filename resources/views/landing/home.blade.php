@extends('layouts.landing')

@section('content')
    <section class="content" style="">
      <div class="bannerHome">
      </div>
    </section>
    <section class="" style="background-color: #fff">
      <div class="container" style="">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 MT500">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 boxHome centered" style="">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 boxHomeChild2 visible-xs">
              <img src="{{ asset('assets/image/banner2.png') }}">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 boxHomeChild">
              <img src="{{ asset('assets/image/logoPaymentku.svg') }}" style="height: 50px">
              <div class="xsMT10" style="font-size: 20px; font-weight: 500; margin-top: 50px">
                <p>Pembayaran Online Yang Aman Bagi<br>Website Anda Dengan Berbagai Saluran<br>Pembayaran Dalam Dan Luar Negeri</p>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 centered xsMT10" style="margin-top:50px">
                <a href="{{ route('register.index') }}" class="btn btn-md btn-orange" style="padding: 5px 35px">DAFTAR SEKARANG</a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 boxHomeChild2 hidden-xs">
              <img src="{{ asset('assets/image/banner2.png') }}">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="blueBgHome">
      <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 centered mTB30">
          <p style="font-size: 26px ; font-weight: 500; color: #fff">Kemudahan Melakukan Pembayaran Online</p>
        </div>
        <div class="col-lg-12 col-xs-12 cardHome" style="">
          <div class="cardHomeIn" style="">
            <div class="cardHomeIn2" style="">
              <div class="thumbnail">
                <img src="{{ asset('assets/image/paymentpoint.svg') }}" style="height: 80px">
                <div class="caption centered">
                  <div style="height: 30px; margin-bottom: 20px">
                    <h6 style="color: #5cbbcc">PEMBAYARAN VIA PAYMENT POIN PAIMENTKU</h6>
                  </div>
                  <p>Pembayaran bisa dilakukan melalui Merchant Paimentku Terdekat</p>
                </div>
              </div>
            </div>
          </div>
          <div class="cardHomeIn" style="">
            <div class="cardHomeIn2" style="">
              <div class="thumbnail">
                <img src="{{ asset('assets/image/transferbank.svg') }}" style="height: 80px">
                <div class="caption centered">
                  <div style="height: 30px; margin-bottom: 20px">
                    <h6 style="color: #5cbbcc">PEMBAYARAN TRANSFER BANK VIRTUAL ACCOUNT</h6>
                  </div>
                  <p>Pembayaran bisa dilakukan dengan Pembayaran Transfer Bank VA</p>
                </div>
              </div>
            </div>
          </div>
          <div class="cardHomeIn" style="">
            <div class="cardHomeIn2" style="">
              <div class="thumbnail">
                <img src="{{ asset('assets/image/kartukredit.svg') }}" style="height: 80px">
                <div class="caption centered">
                  <div style="height: 30px; margin-bottom: 20px">
                    <h6 style="color: #5cbbcc">PEMBAYARAN DENGAN KARTU KREDIT/DEBIT</h6>
                  </div>
                  <p>Pembayaran bisa dilakukan dengan Kartu Kredit/Debit</p>
                </div>
              </div>
            </div>
          </div>
          <div class="cardHomeIn" style="">
            <div class="cardHomeIn2" style="">
              <div class="thumbnail">
                <img src="{{ asset('assets/image/e-wallet') }}.svg" style="height: 80px">
                <div class="caption centered">
                  <div style="height: 30px; margin-bottom: 20px">
                    <h6 style="color: #5cbbcc">E-WALLET</h6>
                  </div>
                  <p>Pembayaran dengan E-wallet</p>
                </div>
              </div>
            </div>
          </div>
          <div class="cardHomeIn" style="">
            <div class="cardHomeIn2" style="">
              <div class="thumbnail">
                <img src="{{ asset('assets/image/internetbanking.svg') }}" style="height: 80px">
                <div class="caption centered">
                  <div style="height: 30px; margin-bottom: 20px">
                    <h6 style="color: #5cbbcc">INTERNET BANKING</h6>
                  </div>
                  <p>Pembayaran dengan Internet Banking</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-home').addClass('active');
    });
  </script>
@endsection
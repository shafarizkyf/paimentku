@extends('layouts.landing')

@section('content')
<section class="content" style="">
  <div class="bannerHome3">
    <div class="bannerBox">
      <div class="container">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
          <div class="col-lg-4 col-sm-5 col-md-4 col-xs-12 layananBox">
            <img src="{{ asset('assets/image/layananinternetbanking.svg') }}">
          </div>
          <div class="col-lg-8 col-sm-7 col-md-8 col-xs-12 layananBox">

            @if(Session::has('success'))
              <h5 class="text-center response text-success">{{ Session::get('success') }}</h5>
            @elseif(Session::has('danger'))
              <h5 class="text-center response text-danger">{{ Session::get('danger') }}</h5>
            @elseif(Session::has('warning'))
              <h5 class="text-center response text-warning">{{ Session::get('warning') }}</h5>
            @endif

            <div class="layananBoxText">Melalui Paymentku anda dapat menyediakan opsi Pembayaran Internet Banking</div>
            <div style="position: absolute; bottom: 20%; right: 0;left: 0;text-align: center;">
              <button class="btn btn-orange" data-toggle="modal" data-target="#daftar" style="padding: 10px 50px">AKTIFKAN SEKARANG</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="daftar" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document" style="width:365px">
      <div class="modal-content" style="border-radius: 0px;">
        <div class="modal-header" style="padding: 5px 15px;border: none;">
          <button type="button" class="close pull-left" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="padding-top: 0px;">
          <form action="{{ route('service.internet_banking.create') }}" method="POST">
            {{ csrf_field() }}
            <div class="formBlock centered" style="padding: 10px 15px">
              <p><b>Silahkan Isi Form Dibawah Ini</b></p>
              <div class="form-group" style="margin-top: 20px">
                <input class="form-control" type="text" name="nama" placeholder="Nama Anda" required />
                @if($errors->has('nama'))
                  <span class="text-danger">{{ $errors->first('nama') }}</span>
                @endif            
              </div>
              <div class="form-group" style="">
                <input class="form-control" type="text" name="telp" placeholder="No. Handphone" required />
                @if($errors->has('telp'))
                  <span class="text-danger">{{ $errors->first('telp') }}</span>
                @endif                        
              </div>
              <div class="form-group" style="">
                <input class="form-control" type="text" name="email" placeholder="E-mail" required />
                @if($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif            
              </div>
              <div class="form-group" style="">
                <input class="form-control" type="text" name="situs" placeholder="Website" required />
                @if($errors->has('situs'))
                  <span class="text-danger">{{ $errors->first('situs') }}</span>
                @endif                        
              </div>
              <div class="form-group">
                <div style="height: 80px;width: 80%;">
                  {!! NoCaptcha::display() !!}
                </div>
                @if($errors->has('g-recaptcha-response'))
                  <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                @endif                                    
              </div>
              <div class="form-group" style="margin-top: 20px">
                <button class="btn btn-block btn-primary" style="border-radius: 0px">Proses</button>
                <button class="btn btn-block btn-default" style="border-radius: 0px" data-dismiss="modal">Batal</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  {!! NoCaptcha::renderJs() !!}
  <script>
    $(function(){
      $('#li-service').addClass('active');
    });
  </script>
@endsection
@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow">
            <div class="pt10 pb20">
              <p class="judul">Billing</p>
              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                        <th>Waktu Transaksi</th>
                        <th>Keterangan</th>
                        <th>order ID</th>
                        <th>Jumlah</th>
                        <th>Refund</th>
                        <th>Biaya Admin</th>
                        <th>Total Bayar</th>
                        <th class="centered">Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($billing as $o)
                    <tr>
                        <td>{{ $o['tanggal'] }}</td>
                        <td>{{ $o['keterangan'] }}</td>
                        <td>{{ $o['id_order'] }}</td>
                        <td>{{ $o['jumlah'] }}</td>
                        <td>{{ $o['refund'] }}</td>
                        <td>{{ $o['admin'] }}</td>
                        <td>{{ $o['total'] }}</td>
                        <td class="centered">
                          <span data-toggle="modal" data-target="#detail">
                            <a type="button" 
                              data-tanggal="{{ $o['tanggal'] }}"
                              data-keterangan="{{ $o['keterangan'] }}"
                              data-id="{{ $o['id_order'] }}"
                              data-jumlah="{{ $o['jumlah'] }}"
                              data-admin="{{ $o['admin'] }}"
                              data-total="{{ $o['total'] }}" 
                              data-toggle="tooltip" 
                              title="Lihat Detail" 
                              class="detailBtn"
                              style="cursor:pointer">Lihat</a>
                          </span>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
        </div>
      </div>
  </div>
  <div class="modal fade" id="detail" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" style="border-radius: 0px;">
            <div class="modal-header" style="padding: 5px 15px;border: none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="font-size: 12px;text-align: right;">Tanggal: {{ date('d/m/Y') }}</div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <img src="{{ asset('assets/image/logoPaymentku.svg') }}">
              </div>
            </div>
            <div class="modal-body" style="padding-top: 0px">
              <div style=" display: table-cell;min-height: 350px">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding centered" >
                    <table  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt20" >
                        <tbody>
                          <tr>
                              <td class="right lh32">Waktu Transaksi</td>
                              <td class="left lh32" id="detail-tanggal"></td>
                          </tr>
                          <tr>
                              <td class="right lh32">Keterangan</td>
                              <td class="left lh32" id="detail-keterangan"></td>
                          </tr>
                          <tr>
                              <td class="right lh32">Order ID</td>
                              <td class="left lh32" id="detail-id"></td>
                          </tr>
                          <tr>
                              <td class="right lh32">Jumlah</td>
                              <td class="left lh32" id="detail-jumlah"></td>
                          </tr>
                          <tr>
                              <td class="right lh32" style="padding-bottom: 20px">Biaya Admin</td>
                              <td class="left lh32" style="padding-bottom: 20px" id="detail-admin"></td>
                          </tr>
                          <tr style="border-top: 3px solid black">
                              <td class="right lh32"><b>Total Biaya</b></td>
                              <td class="left lh32"><b id="detail-total"></b></td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px">
                    <div class="col-xs-6"><a type="button" href="successdaftar.php" class="btn btn-block btn-default" style="border-radius: 0px">Simpan</a></div>
                    <div class="col-xs-6"><a type="button" href="successdaftar.php" class="btn btn-block btn-default" style="border-radius: 0px">Print</a></div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-billing').addClass('active');
      $('.detailBtn').on('click', function(){
        var jumlah = $(this).data('jumlah');
        var tanggal = $(this).data('tanggal');
        var keterangan = $(this).data('keterangan');
        var id = $(this).data('id');
        var admin = $(this).data('admin');
        var total = $(this).data('total');

        $('#detail-jumlah').html(jumlah);
        $('#detail-keterangan').html(keterangan);
        $('#detail-tanggal').html(tanggal);
        $('#detail-id').html(id);
        $('#detail-admin').html(admin);
        $('#detail-total').html(total);

      });
    });
  </script>
@endsection
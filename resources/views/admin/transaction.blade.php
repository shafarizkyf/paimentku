@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow">
           <div class="pt10 pb20">
              <p class="judul">Transaction</p>
              <table id="example1" class="table table-bordered table-striped">
                 <thead>
                    <tr>
                       <th>Waktu Transaksi</th>
                       <th>Keterangan</th>
                       <th>order ID</th>
                       <th>Jumlah</th>
                       <th>Status</th>
                       <th>Tipe Pembayaran</th>
                    </tr>
                 </thead>
                 <tbody>
                   @foreach($transaction as $o)
                    <tr>
                       <td>{{ $o['tanggal'] }}</td>
                       <td>{{ $o['keterangan'] }}</td>
                       <td>{{ $o['id_order'] }}</td>
                       <td>{{ $o['jumlah'] }}</td>
                       <td>{{ $o['status'] }}</td>
                       <td>{{ $o['type'] }}</td>
                    </tr>
                    @endforeach
                 </tbody>
              </table>
           </div>
        </div>
     </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-transaction').addClass('active');
    });
  </script>
@endsection
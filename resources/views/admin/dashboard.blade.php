@extends('layouts.admin')

@section('balance')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
  <a type="button" href="successdaftar.php" data-toggle="modal" data-target="#withdraw" class="btn btn-default btn-sm btn-success pull-right" style="border-radius: 0px">Withdraw</a>
  <span>
    <p class="pull-right lh32 bgWhite" style="margin-right: 20px; padding: 0px 10px;">0</p>
    <p class="pull-right lh32" style="margin-right: 20px">Saldo Rp.</p>
  </span>
 </div>
@endsection

@section('content')
<section class="content mB50">
  <div class="container">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  bgWhite shadow  pt10 pb20">
     <p class="judul">Summary</p>
     <div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-size: 16px; font-weight: 600 ; color: #000">
           <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12  vhCenter" style="height: 150px; border-right: 3px solid black">
              <div class="verticalCenterIn">
                 Total Volume Transaksi <br><i>Month To Date</i>
                 <div>
                    <h4>{{ $summary->tot_volume }}</h4>
                 </div>
              </div>
           </div>
           <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vhCenter" style="height: 150px">
              <div class="verticalCenterIn">
                 Total Transaksi <br><i>Month To Date</i>
                 <div class="centered">
                    <h4>{{ $summary->tot_trans }}</h4>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</section>
<section class="content mB50">
  <div class="container">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow">
        <div class="pt10 pb20" style="max-height: 400px;position: relative;">
           <p class="judul col-lg-6 pull-left">Volume Transaksi</p>
           <div class="col-lg-4 pull-right">
              <select class="form-control select2 left" id="stat-date" style="width: 100%" data-placeholder="Lihat Statistik Untuk :">
                 <option></option>
                 <option value="0">Hari Ini</option>
                 <option value="-7">7 Hari Sebelum</option>
                 <option value="-30">30 Hari Sebelum</option>
                 <option value="30">Bulan Ini</option>
              </select>
           </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mB50">
              <canvas id="volume" data-route="{{ route('dashboard.volume') }}" width="300" height="125"></canvas>
           </div>
        </div>
     </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow">
        <div class="pt10 pb20">
           <p class="judul">History Penarikan</p>
           <table id="example1" class="table table-bordered table-striped responsive">
              <thead>
                 <tr>
                    <th>Tanggal</th>
                    <th>Bank</th>
                    <th>No.Rekening</th>
                    <th>Keterangan</th>
                 </tr>
              </thead>
              <tbody>
                 @foreach($history as $o)
                 <tr>
                    <td>{{ $o['tanggal'] }}</td>
                    <td>{{ $o['bank'] }}</td>
                    <td>{{ $o['rekening'] }}</td>
                    <td>{{ $o['keterangan'] }}</td>
                 </tr>
                 @endforeach
              </tbody>
           </table>
        </div>
     </div>
  </div>
  <div class="modal fade" id="withdraw" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" style="border-radius: 0px;">
           <div class="modal-header mb20" style="padding: 25px 15px;height: 70px;border: none; background-color: #0e6473; color: #fff">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <b>Penarikan Saldo</b>
              </div>
           </div>
           <div class="modal-body" style="padding-top: 0px">
              <form action="{{ route('withdraw.create') }}" method="POST">
                {{ csrf_field() }}
                <div style=" display: table-cell;min-height: 350px">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 left xsNoPadding fw400 xs-center lh32">Jumlah Penarikan</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xsNoPadding">
                            <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="" placeholder="Jumlah Penarikan" required="required" type="text">
                            <p class="small-text pull-left">Saldo Saat Ini</p>
                            <p class="small-text pull-right">0</p>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 left xsNoPadding fw400 xs-center lh32">Bank</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xsNoPadding">
                            <select class="form-control select2 left" style="width: 100%" data-placeholder="Bank">
                              <option></option>
                              <option>BRI</option>
                              <option>Mandiri</option>
                              <option>BCA</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 left xsNoPadding fw400 xs-center lh32">No Rekening</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xsNoPadding">
                            <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="" placeholder="No Rekeneing" required="required" type="text">
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 left xsNoPadding fw400 xs-center lh32">Keterangan</label>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xsNoPadding">
                            <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="" placeholder="Keterangan" required="required" type="text">
                        </div>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide mB50" style="margin-top: 20px">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-block btn-default btn-primary" style="border-radius: 0px">Proses</button>
                          <button class="btn btn-block btn-default" style="border-radius: 0px" data-dismiss="modal">Batal</button>
                        </div>
                      </div>
                  </div>
                </div>
              </form>
           </div>
        </div>
     </div>
  </div>
</section>
@endsection

@section('js-bottom')
<script>
  $(function(){

    $('#li-dashboard').addClass('active');
    var volume = $('#volume');
    var route  = volume.data('route');
    var labels = [];
    var data   = [];

    var volumeChart = new Chart(volume, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          label: '1 Bulan',
          data: data,
          backgroundColor: 'rgba(14,100,115, 0.9)',
          borderColor: 'rgba(14,100,115, 0.9)',
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    });

    volumeData();
    $('#stat-date').on('change', function(){
      var date = $(this).val();
      volumeData(date);
    });

    function volumeData(date=null){
      $.getJSON(route, {date}).done(function(response){
         data = response.map(function(obj){
            return obj.jumlah;
         });

         labels = response.map(function(obj){
            return obj.tanggal;
         });

         volumeChart.data.datasets[0].data = data;
         volumeChart.data.datasets[0].label = '1 Bulan';
         volumeChart.data.labels = labels;
         volumeChart.update();
      });
    }

  });
</script>
@endsection
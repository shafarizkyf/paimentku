@extends('layouts.admin')

@section('content')
<section class="content mB50">
   <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        @if(Session::has('success'))
        <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
        @elseif(Session::has('danger'))
        <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
        @elseif(Session::has('warning'))
        <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
        @endif  
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
            <div class="pt10 pb20 p20 xs-center">
               <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Account</a></li>
                  <li class="active">IP Whistlist</li>
               </ol>
               <p class="judul xs-center">IP Whistlist</p>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20 plr50 xsNoPadding">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ipwhistlistbox vhCenter  mb20 ">
                     <div class="verticalCenterIn">
                        Ini adalah pengaturan pengamanan yang akan membuat MAP HANYA DAPAT DIAKSES  
                        dari alamat IP yang terdaftar dalam tabel di bawah ini. Jika anda memasukkan alamat IP  
                        yang salah, akses ke MAP Anda akan hilang. Pastikan Anda memiliki alamat IP yang tepat  
                        sebelum memasukkan ke dalam whitelist.
                     </div>
                  </div>
                  <h4 class="xs-center">Alamat IP Anda saat ini adalah 192.168.12.33</h4>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                     <form action="{{ route('ip_whitelist.add') }}" method="POST">
                        {{ csrf_field() }}
                        <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-12 noPaddingSide xs-center">IP Address whitelist</label>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb20 xsNoPadding">
                           <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12" name="ip_add" placeholder="IP Address whitelist" required="required" type="text">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 xsNoPadding xs-center">
                           <button class="btn btn-md btn-default btn-primary">Tambahkan</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-account').addClass('active');
    });
  </script>
@endsection
@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      @if(Session::has('success'))
      <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
      @elseif(Session::has('danger'))
      <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
      @elseif(Session::has('warning'))
      <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
      @endif  
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Account</a></li>
            <li class="active">User Management</li>
          </ol>
          <p class="judul">User Management</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID User</th>
                  <th>Nama User</th>
                  <th>Posisi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $o)
                <tr>
                  <td>{{ $o['email'] }}</td>
                  <td>{{ $o['nama'] }}</td>
                  <td>{{ $o['posisi'] }}</td>
                  <td class="centered">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <span data-toggle="modal" data-target="#edit">
                        <button 
                          class="btn btn-secondary bg0 yellow editBtn"
                          href="#" 
                          data-toggle="tooltip" 
                          data-route="{{ route('user_management.update') }}"
                          data-nama="{{ $o['nama'] }}"
                          data-email="{{ $o['email'] }}"
                          data-posisi="{{ $o['id_posisi'] }}"
                          title="Edit User">
                          <span class="fa fa-edit"></span>
                        </button>
                      </span>
                      <span>
                        <form action="{{ route('user_management.delete') }}" method="POST" style="display:inline">
                          {{ csrf_field() }}
                          <input type="hidden" name="email_sub" value="{{ $o['email'] }}">
                          <button onclick="return confirm('Anda akan menghapus user {{ $o['email'] }}?');" class="btn btn-secondary bg0 red">
                            <span class="fa fa-remove"></span>
                          </button>
                        </form>
                      </span>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>

            <div class="col-sm-6 col-xs-12 mT50 pl0">
              <button class="btn col-sm-3 btn-default btn-primary" data-toggle="modal" data-target="#edit" id="addBtn" data-route="{{ route('user_management.add') }}" style="margin-right: 20px">Tambah User</button>
            </div>              
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content" style="border-radius: 0px;">
        <div class="modal-header" style="padding: 15px 15px;border: none">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <b>User Baru</b>
          </div>
        </div>
        <form action="" method="POST" id="form">
          {{ csrf_field() }}
          <div class="modal-body" style="padding-top: 0px">
            <div style=" display: table-cell;min-height: 350px">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">ID User/ Email</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="email_sub" placeholder="Email" required="required" type="text">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Nama User</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="nama_sub" placeholder="Nama User" required="required" type="text">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Aktifkan Akun</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding left">
                    <label class="radio-inline"><input type="radio" name="jenkel" value="Pria" checked>Pria</label>
                    <label class="radio-inline"><input type="radio" name="jenkel" value="Wanit">Wanita</label>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Nomor HP</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="hp" placeholder="Nomor Handphone" required="required" type="text">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Posisi</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <select class="form-control left" name="posisi" style="width: 100%" data-placeholder="Posisi">
                      <option value="">POSISI</option>
                      <option value="1">ADMIN</option>
                      <option value="2">BILLING</option>
                      <option value="3">ADDER</option>
                    </select>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Aktifkan Akun</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding left">
                    <label class="radio-inline"><input type="radio" name="sub_aktif" value="1" checked>Ya</label>
                    <label class="radio-inline"><input type="radio" name="sub_aktif" value="0">Tidak</label>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Hak Akses</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding left">
                    <label class="pr15"><input class="mr10" type="checkbox" name="hak[]" value="1" checked>Dashboard</label>
                    <label class="pr15"><input class="mr10" type="checkbox" name="hak[]" value="2">Transaction</label>
                    <label class="pr15"><input class="mr10" type="checkbox" name="hak[]" value="3">Billing</label>
                    <label class="pr15"><input class="mr10" type="checkbox" name="hak[]" value="4">Account</label>
                    <label class="pr15"><input class="mr10" type="checkbox" name="hak[]" value="5">Setting</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-header" style="padding: 15px 15px;border: none">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <b>Set Password</b>
            </div>
          </div>
          <div class="modal-body" style="padding-top: 0px">
            <div style=" display: table-cell;min-height: 350px">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding centered" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Password Baru</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="new_pass" placeholder="Password Baru" required="required" type="password">
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12 left xsNoPadding fw400 xs-center lh32">Konfirmasi Password Baru</label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xsNoPadding">
                    <input class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12 br0" name="conf_pass" placeholder="Konfirmasi Password Baru" required="required" type="password">
                  </div>
                </div>
              </div>
              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide mB50" style="margin-top: 20px">
                <div class="col-xs-6">
                  <button class="btn btn-default btn-primary" style="border-radius: 0px">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-account').addClass('active');

      $('#addBtn').click(function(){
        var route = $(this).data('route');
        $('#form').attr('action', route);
        $('#form').find('select[name="posisi"]').val('');
      });

      $('.editBtn').click(function(){
        var route = $(this).data('route');
        var nama = $(this).data('nama');
        var email = $(this).data('email');
        var posisi = $(this).data('posisi');

        $('#form').attr('action', route);
        $('#form').find('input[name="email_sub"]').val(email);
        $('#form').find('input[name="nama_sub"]').val(nama);
        $('#form').find('select[name="posisi"]').val(posisi);
      });
    });
  </script>
@endsection
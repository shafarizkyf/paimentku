@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <form class="form-horizontal form-label-left" method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      @if(Session::has('success'))
      <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
      @elseif(Session::has('danger'))
      <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
      @elseif(Session::has('warning'))
      <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
      @endif          
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.index') }}">Home</a></li>
            <li><a href="#">Account</a></li>
            <li class="active">User Profile</li>
          </ol>
          <p class="judul">Profil</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20">
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Nama Lengkap</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="nama" placeholder="Nama Lengkap" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Email</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="email" placeholder="Email" required="required" type="email">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Jenis Kelamin</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <label class="radio-inline"><input type="radio" value="Pria" name="jenkel" checked>Pria</label>
                  <label class="radio-inline"><input type="radio" value="Wanita" name="jenkel">Wanita</label>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">No Handphone</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="hp" placeholder="No Handphone" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Website</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="website" placeholder="Website" required="required" type="text">
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 pb20">
        <div class="pt10 pb20 p20">
          <p class="judul">Set Password</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10">
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Password Lama</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="old_pass" placeholder="Password Lama" type="password">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Password Baru</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="new_pass" placeholder="Password Baru" type="password">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Konfirmasi password Baru</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="conf_pass" placeholder="Konfirmasi Password Baru" type="password">
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xs-12 mT50 pl0">
        <button class="btn col-sm-3 btn-default btn-primary" style="margin-right: 20px">Simpan</button>
        <a type="button" href="{{ route('profile.index') }}" class="btn col-sm-3 btn-default" >Batal</a>
      </div>
    </div>
    </form>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-account').addClass('active');
    });
  </script>
@endsection
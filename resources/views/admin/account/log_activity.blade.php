@extends('layouts.admin')

@section('content')
<section class="content mB50">
   <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
            <div class="pt10 pb20 p20">
               <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Account</a></li>
                  <li class="active">Log Activity</li>
               </ol>
               <p class="judul">Log Activity</p>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20">
                 <form action="{{ route('log_activity.index') }}">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 noPaddingSide">
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-left: 0px;">
                         <input class="form-control" type="text" name="email" value="{{ Request::get('email') }}" placeholder="ID User / Nama User">
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         <input class="form-control" type="text" name="ip" value="{{ Request::get('ip') }}" placeholder="IP Address">
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         <select class="form-control select2 left" name="activity" style="width: 100%" data-placeholder="Activity">
                            <option></option>
                            <option value="add new user" {{ html_entity_decode(Request::get('activity')) == 'add new user' ? 'selected' : null }}>Add New User</option>
                            <option value="confirm billing" {{ html_entity_decode(Request::get('activity')) == 'confirm billing' ? 'selected' : null }}>Confirm Billing</option>
                         </select>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         <button class="btn btn-default btn-primary">Search</button>
                      </div>
                   </div>                   
                 </form>
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Tanggal</th>
                           <th>ID User</th>
                           <th>IP Address</th>
                           <th>Activity</th>
                        </tr>
                     </thead>
                     <tbody>
                       @foreach($activity as $o)
                        <tr>
                           <td>{{ $o['tanggal'] }}</td>
                           <td>{{ $o['email'] }}</td>
                           <td>{{ $o['ip'] }}</td>
                           <td>{{ $o['log'] }}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-account').addClass('active');
    });
  </script>
@endsection
@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">Email Notification Configuration</li>
          </ol>
          <p class="judul">Email Notification Configuration</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20 xsNoPadding ">
              @if(Session::has('success'))
              <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
              @elseif(Session::has('danger'))
              <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
              @elseif(Session::has('warning'))
              <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
              @endif
              <form class="form-horizontal form-label-left" method="POST" action="{{ route('email_notification.update') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Kirim Email Ke Pelanggan</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <label class="pr15"><input class="mr10" type="checkbox" name="send_cust" checked>Pelanggan anda akan mendapatkan notifikasi email terkait transaksi mereka</label>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Kirim Email Ke Saya</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <label class="pr15"><input class="mr10" type="checkbox" name="send_me" checked>Anda akan mendapatkan notifikasi email terkait setiap transaksi</label>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Notifikasi Email</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="notif_email" placeholder="stevenantek@mail.com" required="required" type="text">
                  <p class="small-text">Email ini akan digunakan untuk menerima notifikasi setiap transaksi selesai, baik itu sukses atau gagal. Jika Anda mempunyai beberapa email, gunakan (,) / koma untuk memisahkan tiap emai</p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Email Support</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="notif_support" placeholder="stevenantek@mail.com" required="required" type="text">
                  <p class="small-text">Email ini akan digunakan sebagai email pengirim untuk pelanggan anda</p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Email Development</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="notif_devel" placeholder="stevenantek@mail.com" required="required" type="text">
                  <p class="small-text">Jika terdapat validasi error, BiroPay akan mengirimkan notifikasi ke email ini</p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Email Finance</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="notif_finance" placeholder="Please type your email for finance's contact person" required="required" type="text">
                  <p class="small-text">Jika terdapat validasi error, Paymentku akan mengirimkan notifikasi ke email ini</p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Email Language</label>
                <div class="col-md-4 col-sm-4 col-xs-12 xsNoPadding">
                  <select class="form-control select2 left" style="width: 100%" name="lang" data-placeholder="Email Language">
                    <option value=""></option>
                    <option value="Indonesia">Indonesia</option>
                    <option value="English">English</option>
                  </select>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Customer Service Phone</label>
                <div class="col-md-4 col-sm-4 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="cust_phone" placeholder="09213131313" required="required" type="text">
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mB50 noPaddingSide">
                <button class="btn btn-md btn-default btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-settings').addClass('active');
    });
  </script>
@endsection
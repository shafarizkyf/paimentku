@extends('layouts.admin')

@section('content')
<section class="content mB50">
   <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
            <div class="pt10 pb20 p20">
               <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Setting</a></li>
                  <li class="active">Access Keys</li>
               </ol>
               <p class="judul">Access Keys</p>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20 noPaddingSide xsNoPadding">
                  @if(Session::has('success'))
                  <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
                  @elseif(Session::has('danger'))
                  <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
                  @elseif(Session::has('warning'))
                  <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
                  @endif
                  <h4 class="xs-center">Configurations</h4>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xs-center mb20 noPaddingSide">
                     Anda harus mengetahui Merchant ID and Server Key anda untuk melakukan integrasi dengan Paymentku. Silahkan gunakan Konfigurasi Development ketika anda sedang dalam fase testing. Dan anda dapat mengganti ke Konfigurasi Production jika anda sudah mendapatkan persetujuan dari Bank dan siap untuk go live
                  </div>
                  <h4 class="xs-center">API KEYS</h4>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xs-center mb20 p20" style="border: black 5px solid">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12 left xsNoPadding fw400 xs-center lh32">ID Merchant</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 xsNoPadding lh32">
                           {{ $accessKey->id }}
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12 left xsNoPadding fw400 xs-center lh32">Client Key</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 xsNoPadding lh32">
                           {{ $accessKey->client }}
                        </div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group noPaddingSide">
                        <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12 left xsNoPadding fw400 xs-center lh32">Server Key</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 xsNoPadding lh32">
                           {{ $accessKey->server }}
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 xs-center mb20 p20" style="background-color:#fff3ce ">Key ini di-generate otomatis oleh sistem. Apabila anda perlu mengganti key anda karena alasan tertentu silahkan hubungi kontak support
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-settings').addClass('active');
    });
  </script>
@endsection
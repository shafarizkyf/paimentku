@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">Daily Report</li>
          </ol>
          <p class="judul">Daily Report</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20">
            @if(Session::has('success'))
            <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
            @elseif(Session::has('danger'))
            <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
            @elseif(Session::has('warning'))
            <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
            @endif
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb20 noPaddingSide">
              <p class="col-lg-6 col-md-6 col-sm-6  col-xs-12 noPaddingSide">Apakah Anda Menginginkan Laporan Transaksi Harian?</p>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group" style="text-align: left;">
                <label class="radio-inline"><input type="radio" name="optradio" checked>Ya</label>
                <label class="radio-inline"><input type="radio" name="optradio">Tidak</label>
              </div>
              <form action="{{ route('daily_report.addEmail') }}" method="POST">
                {{ csrf_field() }}
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 0px;">
                  <input class="form-control" type="text" name="send_em" placeholder="Email">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <button class="btn btn-md btn-default btn-primary pull-right">Tambah</button>
                </div>
              </form>
            </div>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Email</th>
                  <th width="15%">action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($emailReport as $o)
                <tr>
                  <td>{{ $o['email'] }}</td>
                  <td class="centered">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <form action="{{ route('daily_report.deleteEmail') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="email" value="{{ $o['email'] }}">
                        <button class="btn btn-secondary bg0 red" data-toggle="tooltip" title="Delete">
                          <span class="fa fa-remove"></span>
                        </button>
                      </form>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingSide mB50">
            <div class="col-xs-6"><a type="button" href="successdaftar.php" class="btn btn-default btn-primary">Simpan</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-settings').addClass('active');
    });
  </script>
@endsection
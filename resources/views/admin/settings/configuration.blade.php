@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">Configuration</li>
          </ol>
          <p class="judul">Setting URL Redirect</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20 xsNoPadding ">
            @if(Session::has('success'))
            <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
            @elseif(Session::has('danger'))
            <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
            @elseif(Session::has('warning'))
            <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
            @endif
            <p>Paymentku membutuhkan URL endpoints untuk beberapa skenario dibawah ini:</p>
            <form class="form-horizontal form-label-left" method="POST" action="{{ route('configuration.update') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Payment Notification URL*</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="pay" placeholder="Http://" required="required" type="text">
                  <p class="small-text">Alamat dimana Midtrans akan mengirim notifikasi melalui HTTP Post Request. E.g http://yourwebsite.com/notification/handling</p>
                  <a type="button" href="#" class="btn btn-md btn-default btn-brown ">Test Notification Endpoint</a>
                  <a type="button" href="#" class="btn btn-md btn-default btn-brown ">See History</a>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Finish Redirect URL*</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="finish" placeholder="Http://" required="required" type="text">
                  <p class="small-text">Pelanggan akan diarahkan ke link ini jika pembayaran telah sukses dilakukan e.ghttp://yourwebsite.com/payment/finish </p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Unfinish Redirect URL</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="unfinish" placeholder="Http://" required="required" type="text">
                  <p class="small-text">Pelanggan akan diarahkan ke link ini jika mengklik button 'Back to Order Website' di halaman pembayaran VT-Web e.g http://yourwebsite.com/payment/unfinish </p>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 xsNoPadding left" for="">Error Redirect URL*</label>
                <div class="col-md-9 col-sm-9 col-xs-12 xsNoPadding">
                  <input class="form-control col-md-7 col-xs-12t" name="error" placeholder="Http://" required="required" type="text">
                  <p class="small-text">Pelanggan akan diarahkan ke link ini jika terjadi kegagalan dalam proses pembayaran di halaman pembayaran VT-Web e.g http://yourwebsite.com/payment/error</p>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mB50 noPaddingSide">
                <button class="btn btn-md btn-default btn-primary pull-right">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-settings').addClass('active');
    });
  </script>
@endsection
@extends('layouts.admin')

@section('content')
<section class="content mB50">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 mb20">
        <div class="pt10 pb20 p20">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">General Setting</li>
          </ol>
          <p class="judul">Business Setting</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10 pb20">
            @if(Session::has('success'))
              <h6 class="text-center response bg-success text-white">{{ Session::get('success') }}</h6>
            @elseif(Session::has('danger'))
              <h6 class="text-center response bg-danger text-white">{{ Session::get('danger') }}</h6>
            @elseif(Session::has('warning'))
              <h6 class="text-center response bg-warning text-white">{{ Session::get('warning') }}</h6>
            @endif
            <form class="form-horizontal form-label-left" method="POST" action="{{ route('general_settings.updateBusinessSettings') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Merk Dagang</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="merk" placeholder="Merk Dagang" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Nama Perusahaan</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="perusahaan" placeholder="Nama Perusahaan" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Nama Direksi</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="nama" placeholder="Nama Direksi" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">No. Tepl Direksi</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="telp" placeholder="No. Tepl Direksi" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Fax</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="fax" placeholder="Fax" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">NPWP Usaha</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="no_npwp" placeholder="NPWP Usaha" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Nama NPWP</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="nama_npwp" placeholder="Nama NPWP" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">URL Merchant</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="url" placeholder="URL Merchant" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Merchant Email</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="email_mer" placeholder="Merchant Email" required="required" type="email">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Time Zone</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <select class="form-control select2 left" name="timezone" style="width: 100%" data-placeholder="Time Zone">
                    <option value=""></option>
                    <option value="UTC">UTC</option>
                    <option value="UTC+2">UTC+2</option>
                    <option value="UTC+3">UTC+3</option>
                    <option value="UTC+4">UTC+4</option>
                    <option value="UTC+5">UTC+5</option>
                    <option value="UTC+6">UTC+6</option>
                    <option value="UTC+7">UTC+7</option>
                    <option value="UTC+8">UTC+8</option>
                  </select>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Business Address</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="alamat" placeholder="Business Address" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Alamat Cabang</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="cabang" placeholder="Alamat Cabang" required="required" type="text">
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mB50 noPaddingSide">
                <button class="btn btn-md btn-default btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bgWhite shadow pt10 pb20">
        <div class="pt10 pb20 p20">
          <p class="judul">Interface Setting</p>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt10">
            <form class="form-horizontal form-label-left" method="POST" action="{{ route('general_settings.updateInterface') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Logo Perusahaan</label>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <input class="form-control col-lg-8 col-md-7 col-xs-12t" name="logo" type="file" required>
                  <p style="font-size: 14px; font-weight: 500px; color: grey">Ukuran MAX. 1MB (Jpg, Png)</p>
                  @if($errors->has('logo'))
                    <span class="text-danger">{{ $errors->first('logo') }}</span>
                  @endif              
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 left" for="">Nama Perusahaan</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12t" name="label" placeholder="Nama Perusahaan" type="text" required>
                    @if($errors->has('label'))
                      <span class="text-danger">{{ $errors->first('label') }}</span>
                    @endif
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mB50 noPaddingSide">
                <button class="btn btn-md btn-default btn-primary pull-right">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $('#li-settings').addClass('active');
    });
  </script>
@endsection
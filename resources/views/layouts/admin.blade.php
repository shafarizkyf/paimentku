<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <title>PaimentKu</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta name="token" content="{{ csrf_token() }}" />
      <!-- Bootstap -->
      <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
      <!-- Font awesome -->
      <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
      <!-- Datatale -->
      <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <!-- select 2 -->
      <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
      <!-- My style -->
      <link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('assets/css/myAdminStyle.css') }}" rel="stylesheet" type="text/css" />
   </head>
   <body style="background-color: #f6f6f6">
      <section class="">
         <nav class="navbar navbar-fixed-top custom-navbar admin" style="height: 60px;">
            <div class="container xsNoPadding">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand navbarBrandHome" href="{{ route('dashboard.index') }}"><img class="" src="{{ asset('assets/image/logoPaymentku.svg') }}"></a>
               </div>
               <div id="navbar" class="collapse navbar-collapse navbar-xs" style="padding-left:10px;padding-right: 10px; z-index: 100; color: #fefefe">
                  <ul class="nav navbar-nav visible-xs" style="margin-top: 5px;">
                     <li class="mrSm15"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                     <li class="mrSm15"><a href="{{ route('transaction.index') }}">Transaction</a></li>
                     <li class="mrSm15"><a href="{{ route('billing.index') }}">Billing</a></li>
                     <li class="dropdown mrSm15">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account &ensp;<i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu dropdown-menu-navbar" >
                           <div class="list-group  dropdown-menu-navbar">
                              <a type="button" href="{{ route('profile.index') }}"  class="list-group-item btn-nvbr">Profile </a>
                              <a type="button" href="{{ route('ip_whitelist.index') }}" class="list-group-item btn-nvbr">IP Wishlist</a>
                              <a type="button" href="{{ route('user_management.index') }}" class="list-group-item btn-nvbr">User Management</a>
                              <a type="button" href="{{ route('log_activity.index') }}" class="list-group-item btn-nvbr">Log Activity</a>
                           </div>
                        </ul>
                     </li>
                     <li class="dropdown mrSm15">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Setting &ensp;<i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu dropdown-menu-navbar" >
                           <div class="list-group  dropdown-menu-navbar">
                              <a type="button" href="{{ route('general_settings.index') }}"  class="list-group-item btn-nvbr">General Settings </a>
                              <a type="button" href="{{ route('access_keys.index') }}" class="list-group-item btn-nvbr">Access Keys</a>
                              <a type="button" href="{{ route('configuration.index') }}" class="list-group-item btn-nvbr">Konfiguration</a>
                              <a type="button" href="{{ route('email_notification.index') }}" class="list-group-item btn-nvbr">Email notification</a>
                              <a type="button" href="{{ route('daily_report.index') }}" class="list-group-item btn-nvbr">Daili Report</a>
                           </div>
                        </ul>
                     </li>
                  </ul>
                  <ul class="nav navbar-nav pull-right-sm navbar-xs" style="margin-top: 5px;">
                     <li class=" mrSm15 pull-right xs-left"><img src=""><i class="fa fa-user fa-3x"></i></li>
                     <li class=" mrSm15" style="text-align: right;">
                        <p>{{ Session::get('userLogin')->email }}
                           <br>
                           {{ Session::get('userLogin')->nama }}
                        </p>
                     </li>
                  </ul>
               </div>
            </div>
         </nav>
      </section>
      <section class="nav" style="min-height: 100px;">
        <div class="container">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mT70 mB50 hidden-xs">
              @yield('balance')
              <div class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2 boxNavbarCustom col-xs-4">
                 <a class="textDecorationNone" href="{{ route('dashboard.index') }}">
                    <div class="verticalCenter iL iM iS iX bgWhite boxShadow" id="li-dashboard">
                       <div class="centered verticalCenterIn">
                          <i class="fa fa-th "></i>
                          <div class="boxCustomText">Dashboard</div>
                       </div>
                    </div>
                 </a>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 boxNavbarCustom">
                 <a class="textDecorationNone" href="{{ route('transaction.index') }}">
                    <div class="verticalCenter iL iM iS iX bgWhite boxShadow" id="li-transaction">
                       <div class="centered verticalCenterIn">
                          <i class="fa fa-random "></i>
                          <div class="boxCustomText">Transaction</div>
                       </div>
                    </div>
                 </a>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 boxNavbarCustom">
                 <a class="textDecorationNone" href="{{ route('billing.index') }}">
                    <div class="verticalCenter iL iM iS iX bgWhite boxShadow" id="li-billing">
                       <div class="centered verticalCenterIn">
                          <i class="fa fa-file-text-o "></i>
                          <div class="boxCustomText">Billing</div>
                       </div>
                    </div>
                 </a>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 boxNavbarCustom dropdown">
                 <a class="textDecorationNone" href="#">
                    <div class="verticalCenter iL iM iS iX bgWhite boxShadow dropbtn" id="li-account">
                       <div class="centered verticalCenterIn">
                          <i class="fa fa-user-secret "></i>
                          <div class="boxCustomText">Account <span class="fa fa-caret-down"></span></div>
                       </div>
                    </div>
                 </a>
                 <div class="dropdown-content" style="margin-top: 10px">
                    <a type="button" href="{{ route('profile.index') }}" >Profile </a>
                    <a type="button" href="{{ route('ip_whitelist.index') }}">IP Wishlist</a>
                    <a type="button" href="{{ route('user_management.index') }}">User Management</a>
                    <a type="button" href="{{ route('log_activity.index') }}">Log Activity</a>
                 </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 boxNavbarCustom dropdown">
                 <a class="textDecorationNone" href="#">
                    <div class="verticalCenter iL iM iS iX bgWhite boxShadow dropbtn" id="li-settings">
                       <div class="centered verticalCenterIn">
                          <i class="fa fa-cogs "></i>
                          <div class="boxCustomText">Setting <span class="fa fa-caret-down"></span></div>
                       </div>
                    </div>
                 </a>
                 <div class="dropdown-content" style="margin-top: 10px">
                    <a type="button" href="{{ route('general_settings.index') }}"  >General Settings </a>
                    <a type="button" href="{{ route('access_keys.index') }}" >Access Keys</a>
                    <a type="button" href="{{ route('configuration.index') }}" >Konfiguration</a>
                    <a type="button" href="{{ route('email_notification.index') }}" >Email notification</a>
                    <a type="button" href="{{ route('daily_report.index') }}" >Daili Report</a>
                 </div>
              </div>
           </div>
        </div>
      </section>      
      @yield('content')

      <section class="footerAdmin">
         <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vhCenter" style="height: 50px;">
               <div class="verticalCenterIn" style="color: #fff;">
                  Copyright <span class="fa fa-copyright"></span> PT Biropay Indoteknologi Global <br>
                  All right reserved
               </div>
            </div>
         </div>
      </section>
      <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.js') }}"></script>
      <script src="{{ asset('assets/js/myAdmin.js') }}"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
      <script src="{{ asset('assets/plugins/Chart.js/Chart.bundle.js') }}"></script>
      <script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>
      <script type="text/javascript">
         $(document).ready(function() {
         	$(".select2").select2();		
         	$("#example1").DataTable({
               paging : true,
               pagingType: 'simple_numbers',
         	   searching : true,
         	   ordering : true,
               responsive:{
                  details: {
                     display: $.fn.dataTable.Responsive.display.childRowImmediate,
                     type: ''
                  }                
               }
         	});
         });
      </script>
      @yield('js-bottom')
   </body>
</html>
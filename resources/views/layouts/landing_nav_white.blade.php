<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PaimentKu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Bootstap -->
    <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font awesome -->
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- My style -->
    <link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      tr {
      height: 50px;
      text-align: center;
      border-top: none !important;
      vertical-align: middle !important;
      }
      td{
      border: none !important;
      vertical-align: middle !important;
      }
      .grey{
      background-color: #f1f2f2;
      }
      .bold{
      font-weight: 600;
      }
    </style>
  </head>
  <body style="background-color: #f6f6f6">
    <section class="">
      <nav class="navbar navbar-fixed-top custom-navbar" style="height: 60px;">
        <div class="container xsNoPadding">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbarBrandHome" href="{{ route('home') }}"><img class="" src="{{ asset('assets/image/logoPaymentku.svg') }}"></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse navbar-xs" style="padding-left:0px; z-index: 100; color: #fefefe">
            @include('layouts.components.navbar')
          </div>
        </div>
      </nav>
    </section>

    @yield('content')

    @include('layouts.components.footer')

    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/myJs.js') }}"></script>
    @yield('js-bottom')
  </body>
</html>
<section class="footer">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt30 xsNoPadding">
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 footerLink" style="color: #fff; margin-bottom: 20px">
        <p><b>Biropay</b><br></p>
        <a href="{{ route('about') }}">Tentang Kami</a><br>
        <a href="{{ route('blog') }}">Blog</a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footerLink" style="color: #fff;margin-bottom:20px">
        <p><b>Bantuan</b><br></p>
        <a href="{{ route('syarat_ketentuan') }}">Syarat dan Ketentuan</a><br>
        <a href="{{ route('kebijakan_privasi') }}">Kebijakan Privasi</a><br>
        <a href="#">Hubungi Kami</a>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 footerLink pull-right xs-left tar xs-tal" style="color: #f6f6f6;">
        Temukan kami &ensp;&ensp;
        <ul class="social-network social-circle" style="display: inline-block;">
          <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="icoFacebook" title="Instagram"><i class="fa fa-instagram"></i></a></li>
          <li><a href="#" class="icoGoogle" title="Youtube"><i class="fa fa-youtube"></i></a></li>
          <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
        </ul>
        <div class="centered" style=" padding:20px">
          <div class="pull-right centered xs-center imagePlayStore">
            Dapatkan Aplikasi Mobile Paymentku
            <a href="#"><img src="{{ asset('assets/image/google.png') }}" style="height: 30px"></a>
            <a href="#"><img src="{{ asset('assets/image/appstore.png') }}" style="height: 30px"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

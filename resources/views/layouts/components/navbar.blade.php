<ul class="nav navbar-nav pull-right-sm navbar-xs" style="margin-top: 5px">
  <li class="mrSm15" id="li-home"><a href="{{ route('home') }}">Home</a></li>
  <li class="dropdown mrSm15" id="li-service">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Layanan &ensp;<i class="fa fa-caret-down"></i></a>
    <ul class="dropdown-menu dropdown-menu-navbar" >
      <div class="list-group  dropdown-menu-navbar">
        <a type="button" href="{{ route('service.payment_point.index') }}" class="list-group-item btn-nvbr">Paymentku Payment Point </a>
        <a type="button" href="{{ route('service.bank_virtual_account.index') }}" class="list-group-item btn-nvbr">Tranfer Bank VA</a>
        <a type="button" href="{{ route('service.debit_credit_card.index') }}" class="list-group-item btn-nvbr">Kartu Kredit/Debit</a>
        <a type="button" href="{{ route('service.ewallet.index') }}" class="list-group-item btn-nvbr">E-Wallet</a>
        <a type="button" href="{{ route('service.internet_banking.index') }}" class="list-group-item btn-nvbr">Internet Banking</a>
      </div>
    </ul>
  </li>
  <li class="mrSm15" id="li-pricing">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pricing &ensp;<i class="fa fa-caret-down"></i></a>
    <ul class="dropdown-menu dropdown-menu-navbar" >
      <div class="list-group  dropdown-menu-navbar">
        <a type="button" href="{{ route('pricing.business_enterprise') }}" class="list-group-item btn-nvbr">Pricing Business & Enterprise</a>
      </div>
    </ul>
  </li>
  <li class="mrSm15" id="li-support"><a href="{{ route('support') }}">Support</a></li>
  <li class="mrSm15" id="li-documentation"><a href="#">Documentation</a></li>
  <li class="mrSm15" id="li-login"><a href="{{ route('login.index') }}">LOGIN</a></li>
  <li class="mrSm15" id="li-register"><a href="{{ route('register.index') }}">DAFTAR</a></li>
</ul>

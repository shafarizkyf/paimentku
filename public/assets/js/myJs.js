$(document).ready(function(){       
    var scroll_start = 0;
    var startchange = $('.content');
    var offset = startchange.offset();
    if (startchange.length){
        $(document).scroll(function() { 
            scroll_start = $(this).scrollTop();
            if(scroll_start > offset.top) {
                $(".navbar-fixed-top").css('background-color', '#17908e');
                $('.navbarBrandHome img').css('margin-top', '0px');
                $('.navbarBrandHome2 img').css('margin-top', '0px');
                $('.navbarBrandHome2 img').css('height', '30px');
                $(".navbar li a").css('color','#fefefe');
            } else {
                $('.navbar-fixed-top').css('background-color', 'transparent');
                $('.navbarBrandHome img').css('margin-top', '50px');
                $('.navbarBrandHome2 img').css('margin-top', '50px');
                $('.navbarBrandHome2 img').css('height', '50px');
                $(".navbar li a").css('color','');
            }
        });
    }
});